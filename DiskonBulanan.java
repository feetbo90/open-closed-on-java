
/**
 * Created by root on 24/06/17.
 **/

public class DiskonBulanan implements GlobalCustomer {
    public String nama;

    public void setName(String nama) {
        this.nama = nama;
    }

    public double getDiskon(double TotalBelanja) {
        if(TotalBelanja >= 200000)
            return TotalBelanja - 20000;
        else
            return  TotalBelanja;
    }
}
