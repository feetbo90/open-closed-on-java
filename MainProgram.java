
/**
 * Created by root on 24/06/17.
 **/

public class MainProgram {

    public static void main(String [] args)
    {
        double TotalBelanjaHarian = 200000;
        double TotalBelanjaBulanan = 1200000;
        DiskonServis diskonServis = new DiskonServis(TotalBelanjaHarian, new DiskonHarian());

        System.out.println("ini untuk diskon Harian");
        System.out.println(diskonServis.GetDiskon());

        DiskonServis diskonBul = new DiskonServis(TotalBelanjaBulanan, new DiskonBulanan());
        System.out.println("ini untuk diskon Bulanan");
        System.out.print(diskonBul.GetDiskon());
    }
}
