
/**
 * Created by root on 24/06/17.
 **/

class DiskonServis {

    private GlobalCustomer globalCustomer;
    private double TotalBelanja;
    public DiskonServis(double TotalBelanja, GlobalCustomer globalCustomer)
    {
        this.TotalBelanja = TotalBelanja;
        this.globalCustomer = globalCustomer;

    }

    public double GetDiskon()
    {
        return this.globalCustomer.getDiskon(TotalBelanja);
    }
}
