
/**
 * Created by root on 24/06/17.
 **/

public class DiskonHarian implements GlobalCustomer{

    public String nama;

    public void setName(String nama) {
        this.nama = nama;
    }

    public double getDiskon(double TotalBelanja) {
        if(TotalBelanja >= 20000)
            return TotalBelanja - 2000;
        else
            return  TotalBelanja;
    }
}
